//Necessary for OSC communication with SATIE:
import oscP5.*;
import netP5.*;
OscP5 oscReceiver;
NetAddress oscDestination;

String projectRoot;
String[] audioBundles = {"Instrumental", "mots et melismes", "rapides", "lents"};
HashMap<String, Object> audioCollections;

// 
String INSTRUMENTAL_PATH = "/home/pi/Desktop/Sons/Instrumental/";
String MOTS_PATH = "/home/pi/Desktop/Sons/mots et melismes/";
String[] instrumental_files = {"Anandasphère_2.R.wav",  "moment_1.R.wav",  "moment_3.R.wav",
"Anandasphère_7.R.wav",  "moment_2.R.wav"};
String[] mots_files = {"Chaleur réconfortante.wav" ,     "joyfull melism1.wav",     "wonky melism2.wav",
"Granulatore voix graves.L.wav" ,  "liberation.wav" ,          "wonky melism.wav",
"i'm gona be late.wav"  ,         "Lumpa va chercher.wav",
"Je me sens raplapla.wav",        "Mental fragments.L.wav"};

//Parameters to control via wekinator:
float azimuth = 0.5; // -180 to 180 ; 0
float rate = 1.0; // 0.0 to 1.0
float lpf = 18000.0;
float gain1 = 0.0;
float gain2 = 0.0;
boolean loaded = false;
int currentSample;

void setup() {
  //Initialize OSC communication
  oscReceiver = new OscP5(this,12000); //listen for OSC messages on port 12000 (Wekinator default)
  oscDestination = new NetAddress("localhost",18032);
  // initialize the drawing window
  size( 512, 200, P3D );
  projectRoot = getProjectRoot();
  // loadSample();
  loadSamples();
  instantiateSample("instr", instrumental_files[3]);
  instantiateSample("mot", mots_files[3]);
}

void draw() {
  if (!loaded) {
    loaded = true; 
  }
  background( 0 );
}

String getProjectRoot() {
  String ret;
  String[] dirs = sketchPath().split("/");
  String[] rootPath = new String[dirs.length - 1];
  for(int i = 0; i < dirs.length - 1; i++){
    rootPath[i] = dirs[i];
  };
  ret = join(rootPath, "/");
  return ret+"/";
};

void mouseMoved()
{
   azimuth = map( mouseX, 0, width, -180, 180 );
   rate= map( mouseY, 0, height, -1, 1 );
   // controlSample();
   // currentSample = int(map(mouseX, 0, width, 0, float(instrumental_files.length -1)));
   // changeSample("boo", instrumental_files[currentSample]);
   // controlSample("instr", -20);
}

//This is called automatically when OSC message is received
void oscEvent(OscMessage theOscMessage) {
 if (theOscMessage.checkAddrPattern("/wek/outputs")==true) {
     if(theOscMessage.checkTypetag("fff")) { 
        float receivedAzimuth = constrain(theOscMessage.get(0).floatValue(), 0.0, 1.0); //get this parameter
        azimuth = map(receivedAzimuth, 0.0, 1.0, -180, 180);
        
        float receivedRate = constrain(theOscMessage.get(1).floatValue(), 0.0, 1.0); //get 2nd parameter
        rate = map(receivedRate, 0.0, 1.0, -2.0, 2.0);
        
        float receivedGain = constrain(theOscMessage.get(2).floatValue(), 0.0, 0.999); //get third parameters
        gain1 = map(receivedGain, 0.0, 1.0, -49, -5.0);
        gain2 = map(receivedGain, 0.0, 1.0, -5.0, -49);
        
        println("Received new params value from Wekinator");  
      } else {
        println("Error: unexpected params type tag received by Processing");
      }
      // controlSample("boo");
      // controlLPF();
      controlSample("instr", gain1);
      controlSample("mot", gain2);
 }
}

void changeSample(String name, String sample_name) {
  instantiateSample(name, sample_name);
}

void controlLPF(String name) {
  OscMessage msg = new OscMessage("/satie/source/set");
  msg.add(name); 
  msg.add("lpHz");
  msg.add(lpf);
  oscReceiver.send(msg, oscDestination);
}

void controlSample(String name, float gain) {
  OscMessage msg = new OscMessage("/satie/source/set");
  msg.add(name); 
  msg.add("aziDeg");
  msg.add(azimuth);
  msg.add("rate");
  msg.add(rate);
  msg.add("gainDB");
  msg.add(gain);
  oscReceiver.send(msg, oscDestination);
}

void loadSample() {
  OscMessage msg = new OscMessage("/satie/loadSample");
  msg.add("african1"); 
  msg.add("/home/pi/Desktop/Sons/Instrumental/moment 1.R.wav");
  oscReceiver.send(msg, oscDestination);
}

void loadSamples() {
  for (int i = 0; i < instrumental_files.length; i++){
    String file = instrumental_files[i];
    OscMessage msg = new OscMessage("/satie/loadSample");
    msg.add(instrumental_files[i]); 
    msg.add(INSTRUMENTAL_PATH+file);
    oscReceiver.send(msg, oscDestination);
  }
  for (int i = 0; i < mots_files.length; i++){
    String file = mots_files[i];
    OscMessage msg = new OscMessage("/satie/loadSample");
    msg.add(mots_files[i]); 
    msg.add(MOTS_PATH+file);
    oscReceiver.send(msg, oscDestination);
  }
}


void instantiateSample(String name, String filename) {
  OscMessage msg = new OscMessage("/satie/scene/createSource"); // mySource sndBuffer default bufnum mySample gate 1 gainDB -20
  msg.add(name);
  msg.add("sndBuffer");
  msg.add("default");
  msg.add("bufnum");
  msg.add(filename);
  msg.add("gate");
  msg.add(1);
  msg.add("gainDB");
  msg.add(-10);
  oscReceiver.send(msg, oscDestination);
}

void clearScene(){
  OscMessage msg = new OscMessage("/satie/scene/clear");
  oscReceiver.send(msg, oscDestination);
}

void keyPressed() {
  if (key == ESC) {
    clearScene();
  }
}
